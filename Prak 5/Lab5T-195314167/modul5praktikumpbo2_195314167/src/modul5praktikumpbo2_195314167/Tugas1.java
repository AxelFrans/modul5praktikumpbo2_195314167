package modul5praktikumpbo2_195314167;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Tugas1 extends JFrame implements ActionListener {

    private JButton hitung, dialog_ok;
    private JLabel panjang, lebar, luas, dialog_ErrorMessage;
    private JDialog dialogError;
    private JTextField txt_panjang, txt_lebar, txt_luas;
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;

    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
        frame.setResizable(false);
    }

    public Tugas1() {
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Luas Tanah");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        panjang = new JLabel("Panjang(m)");
        panjang.setBounds(20, 20, 60, 20);
        contentPane.add(panjang);
        txt_panjang = new JTextField();
        txt_panjang.setBounds(120, 20, 100, 20);
        contentPane.add(txt_panjang);

        lebar = new JLabel("Lebar(m)");
        lebar.setBounds(20, 50, 60, 20);
        contentPane.add(lebar);
        txt_lebar = new JTextField();
        txt_lebar.setBounds(120, 50, 100, 20);
        contentPane.add(txt_lebar);

        luas = new JLabel("Luas(m^2)");
        luas.setBounds(20, 80, 80, 20);
        contentPane.add(luas);
        txt_luas = new JTextField();
        txt_luas.setBounds(120, 80, 100, 20);
        txt_luas.setEditable(false);
        contentPane.add(txt_luas);

        hitung = new JButton("Hitung");
        hitung.setBounds(120, 120, 80, 20);
        contentPane.add(hitung);
        hitung.addActionListener(this);

        dialog_ok = new JButton("OK");
        dialog_ok.setBounds(120, 55, 80, 20);

        dialog_ErrorMessage = new JLabel("Maaf,hanya integer yang diperbolehkan");
        dialog_ErrorMessage.setBounds(50, 10, 340, 40);

        dialogError = new JDialog();
        dialogError.setBounds(120, 300, 340, 110);
        dialogError.setLayout(null);
        dialogError.setResizable(false);
        dialogError.setTitle("Error");
        dialogError.add(dialog_ok);
        dialogError.add(dialog_ErrorMessage);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        try {

            int width = Integer.parseInt(txt_panjang.getText());
            int height = Integer.parseInt(txt_lebar.getText());
            int extent = width * height;
            this.txt_luas.setText(Integer.toString(extent));

        } catch (Exception e) {
            dialogError.setVisible(true);
            

        }

    }

}
